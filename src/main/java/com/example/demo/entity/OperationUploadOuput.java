package com.example.demo.entity;


import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class OperationUploadOuput {

	private String identifier;
	private String uploadEnpoint;
	private String data;
	
	
}
