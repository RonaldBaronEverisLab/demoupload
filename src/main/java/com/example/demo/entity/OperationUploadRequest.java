package com.example.demo.entity;

import java.util.List;

import lombok.Data;


@Data
public class OperationUploadRequest {
	
	public  List<OperationUploadInput>  items;

}
