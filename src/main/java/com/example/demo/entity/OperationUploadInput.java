package com.example.demo.entity;

import java.util.Date;

import lombok.Data;

@Data
public class OperationUploadInput{

	 private String identifier;
	 private String name;
	 private String description;
	 private Date date;
	 private String operation;
}
