package com.example.demo.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;


import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.entity.OperationUploadInput;
import com.example.demo.service.Services;
import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.blob.CloudBlobContainer;
import com.microsoft.azure.storage.blob.CloudBlockBlob;
import com.microsoft.azure.storage.blob.SharedAccessBlobPolicy;

@RestController
public class UploadSas {

	
	public Services services;	

	@SuppressWarnings("finally")
	@RequestMapping(value = "/urlSAS", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<Object> uploadFile(@RequestParam("file") MultipartFile file) throws IOException {

		services.credentialsMetada();

		File convertFile = new File("/home/rbaronsa/Escritorio/Files/" + file.getOriginalFilename());
		convertFile.createNewFile();
		CloudBlobContainer container = null;

		try {

			container = services.getBlobContainer();

			FileOutputStream fout = new FileOutputStream(convertFile);
			fout.write(file.getBytes());
			fout.close();
			
	
			CloudBlockBlob blob = container.getBlockBlobReference("archivo1/" + file.getOriginalFilename());
			blob.uploadFromFile(convertFile.getAbsolutePath());
			
			/*---------------------------------------------------------*/
			SharedAccessBlobPolicy policy = new SharedAccessBlobPolicy();  //Póliticas de Blob de acceso compartido..
			policy.setPermissionsFromString("rcw"); //Establece permisos de acceso compartido utilizando la cadena de permisos especificada.
			Calendar date = Calendar.getInstance();
			Date expire = new Date(date.getTimeInMillis() + (30 * 60000)); //Tiempo de expiración
			Date start = new Date(date.getTimeInMillis());
			policy.setSharedAccessExpiryTime(expire); //Representa el tiempo de caducidad para establecer la firma de acceso compartido.
			policy.setSharedAccessStartTime(start);  //Representa la hora de inicio para establecer la firma de acceso compartido.
			
			
			//Uri generada por firmas de acceso SAS
			String url=blob.getUri().toString()+"?"+blob.generateSharedAccessSignature(policy, null);
			System.out.println("url: "+url);
			
			OperationUploadInput p=new OperationUploadInput();
			p.setName(file.getOriginalFilename());
			System.out.println(p.getName());
			

		} catch (StorageException ex) {
			return new ResponseEntity<>("Error devuelto por el servicio. Código Http", HttpStatus.OK);
		} catch (Exception ex) {
			return new ResponseEntity<>("Error excepción..", HttpStatus.OK);
		} finally {
			return new ResponseEntity<>("El programa a completado con éxito", HttpStatus.OK);
		}

	}
	
	
}
