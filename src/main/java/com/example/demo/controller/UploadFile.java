package com.example.demo.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.service.Services;
import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.blob.CloudBlobContainer;
import com.microsoft.azure.storage.blob.CloudBlockBlob;


@RestController("/api")
public class UploadFile {

	@Autowired
	public Services services;

	@SuppressWarnings("finally")
	@RequestMapping(value = "/upload", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<Object> uploadFile(@RequestParam("file")MultipartFile file) throws IOException {
	
		services.credentialsMetada();

		File convertFile = new File("/home/rbaronsa/Escritorio/Files/" + file.getOriginalFilename());

		convertFile.createNewFile();

		CloudBlobContainer container = null;

		try {
			container = services.getBlobContainer();

			FileOutputStream fout = new FileOutputStream(convertFile);
			fout.write(file.getBytes());
			fout.close();
			
	
			/*operationUpload.setDate(new Date());
			operationUpload.setDescription(operationUpload.getDescription());
			operationUpload.setIdentifier(operationUpload.getIdentifier());*/
			
			CloudBlockBlob blob = container.getBlockBlobReference("archivo1/" + file.getOriginalFilename());
			blob.uploadFromFile(convertFile.getAbsolutePath());
			
			
			

		} catch (StorageException ex) {
			return new ResponseEntity<>("Error devuelto por el servicio. Código Http", HttpStatus.OK);
		} catch (Exception ex) {
			return new ResponseEntity<>("Error excepción..", HttpStatus.OK);
		} finally {
			return new ResponseEntity<>("El programa a completado con éxito", HttpStatus.OK);
		}
		
		
		
		
		

	}

}
