package com.example.demo.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.entity.OperationUploadInput;
import com.example.demo.entity.OperationUploadOuput;
import com.example.demo.entity.OperationUploadRequest;
import com.example.demo.entity.OperationUploadResponse;
import com.example.demo.service.Services;
import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.blob.CloudBlobClient;
import com.microsoft.azure.storage.blob.CloudBlobContainer;
import com.microsoft.azure.storage.blob.CloudBlockBlob;
import com.microsoft.azure.storage.blob.SharedAccessBlobPolicy;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/verificar")
@Slf4j
public class UploadToken {

	@Autowired
	public Services services;
	
	//@Autowired
	//private OperationUploadRepository repo;
	

	
	@SuppressWarnings("finally")
	@PutMapping
	public ResponseEntity<Object> uploadToken(@RequestBody OperationUploadInput operationUpload) throws IOException{
		
		services.credentialsMetada();
		MultipartFile file=null;
		
		@SuppressWarnings("null")
		File convertFile = new File("/home/rbaronsa/Escritorio/Files/" + file.getOriginalFilename());
		
		operationUpload.setName(file.getOriginalFilename());
		
		convertFile.createNewFile();
		CloudBlobContainer container = null;
		

		try {

			container = services.getBlobContainer();

			FileOutputStream fout = new FileOutputStream(convertFile);
			fout.write(file.getBytes());
			fout.close();
			
	       
			CloudBlockBlob blob = container.getBlockBlobReference("archivo1/" + file.getOriginalFilename());
			blob.uploadFromFile(convertFile.getAbsolutePath());  //cargar contenedor y archivo
			
			/*---------------------------------------------------------*/
			SharedAccessBlobPolicy policy = new SharedAccessBlobPolicy();  //Póliticas de Blob de acceso compartido..
			policy.setPermissionsFromString("rcw"); //Establece permisos de acceso compartido utilizando la cadena de permisos especificada.
			Calendar date = Calendar.getInstance();
			Date expire = new Date(date.getTimeInMillis() + (30 * 60000)); //Tiempo de expiración
			Date start = new Date(date.getTimeInMillis());
			policy.setSharedAccessExpiryTime(expire); //Representa el tiempo de caducidad para establecer la firma de acceso compartido.
			policy.setSharedAccessStartTime(start);  //Representa la hora de inicio para establecer la firma de acceso compartido.
			
			//Uri generada por firmas de acceso SAS
		//	String url=blob.getUri().toString()+"?"+blob.generateSharedAccessSignature(policy, null);
			//System.out.println("url: "+url);
			
			
				
			operationUpload.setName(file.getOriginalFilename()); //Se seteo el nombre del archivo a cargar
			
			/*operationUpload.setDate(new Date());
			operationUpload.setDescription(operationUpload.getDescription());
			operationUpload.setIdentifier(operationUpload.getIdentifier());*/
			
			
			
			

		} catch (StorageException ex) {
			return new ResponseEntity<>("Error devuelto por el servicio. Código Http", HttpStatus.OK);
		} catch (Exception ex) {
			return new ResponseEntity<>("Error excepción..", HttpStatus.OK);
		} finally {
			return new ResponseEntity<>("El programa a completado con éxito", HttpStatus.OK);
		}
		
		
	}
	
	
	@SuppressWarnings("finally")
	@RequestMapping(value="/entrada",method = RequestMethod.POST)
	public ResponseEntity<OperationUploadResponse>   postController(@RequestBody  OperationUploadRequest operationUploadRequest){
	
		OperationUploadResponse operationUploadResponse=new OperationUploadResponse();
	
		List<OperationUploadOuput> outputItems=new ArrayList<OperationUploadOuput>();
	   
	    for(OperationUploadInput item : operationUploadRequest.items) {
	    	OperationUploadOuput ouput=new OperationUploadOuput();
	    	ouput.setIdentifier(item.getIdentifier());
	    	
	    	/* BEGIN: generate Url sas token*/
	    	String uploadEnpoint="";
	     
	    	services.credentialsMetada();
	    	CloudBlobContainer container = null;
	    	

			try {
				container = services.getBlobContainer();

			    String blobKey = "sasfile-"+UUID.randomUUID().toString()+".txt";

				CloudBlockBlob blob = container.getBlockBlobReference(blobKey); //Representa un blob que se carga como un conjunto de bloques.
				
				
				
				SharedAccessBlobPolicy policy = new SharedAccessBlobPolicy();  //Póliticas de Blob de acceso compartido..
				policy.setPermissionsFromString("rcw"); //Establece permisos de acceso compartido utilizando la cadena de permisos especificada.
				Calendar date = Calendar.getInstance();
				Date expire = new Date(date.getTimeInMillis() + (30 * 60000)); //Tiempo de expiración
				Date start = new Date(date.getTimeInMillis());
				policy.setSharedAccessExpiryTime(expire); //Representa el tiempo de caducidad para establecer la firma de acceso compartido.
				policy.setSharedAccessStartTime(start);  //Representa la hora de inicio para establecer la firma de acceso compartido.
				
				
				//Uri generada por firmas de acceso SAS
				uploadEnpoint =blob.getUri().toString()+"?"+blob.generateSharedAccessSignature(policy, null);
				ouput.setUploadEnpoint(uploadEnpoint);
				System.out.println("url: "+uploadEnpoint);
				outputItems.add(ouput);
				operationUploadResponse.setItems(outputItems);
				
			} catch (StorageException ex) {
				
			} catch (Exception ex) {
				
			} finally {
				return new ResponseEntity<>(operationUploadResponse, HttpStatus.OK);
			}
	    		
				
				/* END: generate Url sas token*/
	    	
	    }
		return new ResponseEntity<>(operationUploadResponse, HttpStatus.OK);
	   
	
		
		
	}
	

	
  /*
    @GetMapping
	public List<OperationUploadInput> listar() throws IOException{
        
		return repo.findAll();
	}
    
    @PostMapping
    public void  insertar(@RequestBody OperationUploadInput op){
		repo.save(op);
	}
	*/
	
	/*@PostMapping()
	public ResponseEntity<?> insertarUpload(@RequestBody OperationUploadRequest opeRequest){
	     	
		
		
	}*/
	
	
	
	
	
	
	
}



	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	