package com.example.demo.pruebasRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/post")
public class Controller {
	
		@PostMapping("/login")
		@ResponseBody
		public  ResponseEntity<?> login(@RequestBody User user) {
			
			return ResponseEntity.ok(HttpStatus.OK);
		
		}
}
